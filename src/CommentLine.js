import React, { Component } from 'react';
import dateformat from 'dateformat';
import SmileyFaceDisplay from './smileyFaceDisplay';
import * as watson from './util/watsonCalls';
import {getQualitativeScore} from './util/utilityMethods';
import './App.css';

class CommentLine extends Component {

  state = {
    score: this.props.score
  };

  

  getSentimentForText()
  {
    watson.getSentimentForText(this.props.comment)
    .then(res => res.text())
    .then(sentimentResults => {
      const sentimentObj = JSON.parse(sentimentResults);
      const curScore = Math.round(10 * sentimentObj.sentiment.document.score);
      this.setState({ score: curScore })
      this.props.updateAverage(curScore);  
    });
      //update comment in state
  }
  
  componentDidMount()
  {
    this.getSentimentForText();
  }

  render() {
    const createdString = dateformat(this.props.created, "yyyy-mm-dd hh:MM");
    const qualitativeSentiment = getQualitativeScore(this.state.score);
    return (
      <div className="commentLine">
      <div className="commentDateLine">
        {createdString}
        </div>
        <div className="commentText">&ldquo;{this.props.comment}&rdquo;</div>
        {
          (this.state.score === 'loading') ? (
            <div className="smileyBox" >
              loading...
            </div>
          ) : (
            <div className="smileyBox" >
            <SmileyFaceDisplay className="smallImage" score={this.state.score} />
            <div className="sentimentLabel">{qualitativeSentiment}</div>
            </div>
          )
         
        }
       

      </div>
    );
  }
}

export default CommentLine;
