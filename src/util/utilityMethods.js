
// @param - `eventsObject` is raw data from Github
// this method parses that object, finding all events with a message body

export const getCommentsFromEvents = (eventsObject) => {
    if (!(eventsObject instanceof Array)){
        return [];
    }
    //just getting events with text
    const userComments = eventsObject.filter(anEvent => {
        return (anEvent.hasOwnProperty('payload') && 
            (anEvent.payload.hasOwnProperty('comment') || anEvent.type === 'PullRequestEvent') )  
    }).map(commentEvent => {
        const commentData = {};
        // detecting and coalescing different object types into simpler object format... 
        if(commentEvent.payload.hasOwnProperty('comment'))
        {
            commentData.body =  commentEvent.payload.comment.body;
            commentData.created_at = commentEvent.payload.comment.created_at;
        }else{
            commentData.body =  commentEvent.payload.pull_request.body;
            commentData.created_at = commentEvent.payload.pull_request.updated_at;

        }
        
        return commentData;
    })
    return userComments;
}

export const getQualitativeScore = (score) =>
{
    const scoreToWords = [
        'Basically horrible',
        'Vicious',
        'Mean',
        'Rude',
        'Cranky',
        'Short',
        'Unpleasant',
        'Neutral',
        'Pleasant',
        'Nice',
        'Amiable',
        'Friendly',
        'Very Friendly',
        'Extremely Friendly'
    ];
    const negativeOffset = 7;

    //trim limits
    if(score < -7) {
        score = -7;
      }
    if(score > 6) {
        score = 6;
      }

    return scoreToWords[(score + negativeOffset)];
}