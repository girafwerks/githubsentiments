
# Github Sentiment Analyzer
- A tool that analyzes the friendliness of github users, based on a sentiment analysis of their comments

## To install:
`npm install`

## To start:
`npm start`


### TODO: 

ESSENTIAL:
- retry actual calls to Watson APIs

WOULDBENICE:
- add some unit tests
- what _other_ types of comments can we get? -- issue reports?, others? etc
- create separate component for each 
- can i do sentiment analysis in bulk?? 
- tidy: separate the css for each component
- show dates in comments
- add linter to project
- authenticate to GH for rate limit bump
- use far more beautiful new GraphQL API from Github?
- embedded comments shouldnt be passed to sentiment analyzer
- truncate or split long strings

### Image Credits
https://www.iconfinder.com/search/?q=iconset%3Ahawcons+face&price=free&style=outline