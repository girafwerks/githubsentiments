import {localConfig} from '../config';

//const apiKey = localConfig.watson_api.key;
const watsonBaseUrl = `${localConfig.watson_api.protocol}${localConfig.watson_api.host}`;
const sentimentUrl = `${watsonBaseUrl}/natural-language-understanding/api/v1/analyze?version=${localConfig.watson_api.api_version}`;

export const getSentimentForText = (givenText) => 
{   
    console.log('getting score for:', givenText);
    const requestParams = {  "text": givenText,
                             "features": {
                                "sentiment": {}
                              } }; 
    let headerObj = new Headers({
        "Authorization": 'Basic YXBpa2V5Okxxc19GZndYOVJtV2FXSVFvZE51THNTVEU0bk1nd1M0Qkx6Z2xmTEE2MzNU',
        "Content-Type": "application/json"
      })
      
    return fetch('https://cors-anywhere.herokuapp.com/' + sentimentUrl, 
        {   method: "POST", 
            body:  JSON.stringify(requestParams),
            //mode: "no-cors", 
            headers: headerObj
        } )
    }
