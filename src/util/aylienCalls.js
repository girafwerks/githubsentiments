import {localConfig} from '../config';

const apiKey = localConfig.aylien_api.key;
const aylienBaseUrl = `${localConfig.aylien_api.endpoint}`;
const sentimentUrl = `${aylienBaseUrl}/api/v1/sentiment`;

/* example Alyien call: 
curl https://api.aylien.com/api/v1/sentiment \
   -H "X-AYLIEN-TextAPI-Application-Key: YOUR_APP_KEY" \
   -H "X-AYLIEN-TextAPI-Application-ID: YOUR_APP_ID" \
   -d mode="tweet" \
   -d text="John+is+a+very+good+football+player"
*/

export const getSentimentForText = (givenText) => 
{
    let headerObj = new Headers({
        "X-AYLIEN-TextAPI-Application-Key": localConfig.aylien_api.key,
        "X-AYLIEN-TextAPI-Application-ID": localConfig.aylien_api.appid
      });
      
      let literalHeaders = {
        'X-AYLIEN-TextAPI-Application-Key': apiKey,
        'X-AYLIEN-TextAPI-Application-ID': localConfig.aylien_api.appid
      };

/*curl https://api.aylien.com/api/v1/sentiment \
   -H "X-AYLIEN-TextAPI-Application-Key: 217363ef3d82a525dd4800c5700f610a" \
   -H "X-AYLIEN-TextAPI-Application-ID: ea2ef03a" \
   -d mode="tweet" \
   -d text="John+is+a+very+good+football+player"*/
    
    fetch(`${'https://cors-anywhere.herokuapp.com/'}${sentimentUrl}`, {
         method: "POST", 
         headers: headerObj, 
         mode:'no-cors' 
        } )
    .catch(error => { console.log(error) })
    .then(res => res.text())
    .then(body => console.log(body));
}
